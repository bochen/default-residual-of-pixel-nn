#!/usr/bin/env python

import os,sys
import ROOT
import math

from ROOT import TFile, gDirectory
from ROOT import TH1F
from ROOT import *

ROOT.gROOT.LoadMacro("/afs/cern.ch/work/b/bochen/ATLASstyle/atlasrootstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/b/bochen/ATLASstyle/atlasrootstyle/AtlasLabels.C")
SetAtlasStyle()

xresidual = ROOT.TH1F("xresidual","xresidual",50,-0.05,0.05)
yresidual = ROOT.TH1F("yresidual","yresidual",50,-0.05,0.05)

def fitplot():
    myfile = TFile("NNinput_pos2_test.root")
    mychain = gDirectory.Get( 'NNinput' )
    entries = mychain.GetEntriesFast()
    for jentry in xrange( entries ):
        nb = mychain.GetEntry( jentry )
        if nb <= 0:
            continue
        xtruth0=mychain.NN_position_id_X_0/1000.
        ypred0=mychain.NN_position_Y_0/1000.
        ytruth0=mychain.NN_position_id_Y_0/1000.
        xpred1=mychain.NN_position_X_1/1000.
        xtruth1=mychain.NN_position_id_X_1/1000.
        ypred1=mychain.NN_position_Y_1/1000.
        ytruth1=mychain.NN_position_id_Y_1/1000.
    # for 3-particle
    #    xpred2=mychain.NN_position_X_2/1000.
    #    xtruth2=mychain.NN_position_id_X_2/1000.
    #    ypred2=mychain.NN_position_Y_2/1000.
    #    ytruth2=mychain.NN_position_id_Y_2/1000.
        xresidual.Fill(xpred0-xtruth0)
        xresidual.Fill(xpred1-xtruth1)
        yresidual.Fill(ypred0-ytruth0)
        yresidual.Fill(ypred1-ytruth1)
    canvasx=ROOT.TCanvas("canvasx", "canvasx", 800, 600)
    frame= ROOT.TH1F("frame","frame",50,-0.05,0.05)
    frame.SetMaximum(0.13)
    frame.SetMinimum(0.00)
    frame.SetXTitle("2-particle Truth hit X residuals [pixel index]")
    frame.GetXaxis().SetTitleSize(0.04);
    frame.GetXaxis().SetLabelSize(0.04);
    frame.GetXaxis().SetTitleOffset(1);
    frame.GetYaxis().SetTitleSize(0.04);
    frame.GetYaxis().SetTitleOffset(1.1);
    frame.GetYaxis().SetLabelSize(0.04);
    frame.SetYTitle("Cluster density")
    frame.Draw()
    xresidual.DrawNormalized("hist same")
    canvasx.SaveAs("xresidual.pdf")
    canvasy=ROOT.TCanvas("canvasy", "canvasy", 800, 600)
    frame.SetXTitle("2-particle Truth hit Y residuals [pixel index]")
    frame.Draw()
    yresidual.DrawNormalized("hist same")
    canvasy.SaveAs("yresidual.pdf")


fitplot()
